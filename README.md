# Test YUKK Indonesia - Full Stack Developer


Di buat oleh [Muhamad Sadan Dwi Cahyo](https://www.linkedin.com/in/muhamad-sadan-dwi-cahyo-48983a129/). Template by [Creative Team](https://www.creative-tim.com/product/soft-ui-dashboard-pro-tailwind). 

## Website menggunakan TALL Stack :
* [Tailwind](https://tailwindcss.com)
* [Alphine.js](https://alpinejs.dev/)
* [Laravel](http://laravel.com)
* [Livewire](https://laravel-livewire.com/)

## Requirements
* Mysql Database
* Php 8+
* Composer 2+

## How to run
- `composer install` untuk menginstall dependencies
- `php artisan migrate` untuk menjalankan migrasi ke db
- `php artisan db:seed` 
- `npm install`
- `npm run dev atau build`
- `php artisan serve`

default user/password login dari seeder yang telah diberikan :
admin@admin.com / password