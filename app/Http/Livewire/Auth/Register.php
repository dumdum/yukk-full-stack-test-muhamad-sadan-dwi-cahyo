<?php

namespace App\Http\Livewire\Auth;

use App\Models\User;
use App\Models\Account;

use Livewire\Component;
use Illuminate\Support\Facades\Hash;

class Register extends Component
{
    public $name = '';
    public $email = '';
    public $password = '';

    protected $rules = [
        'name' => 'required|min:3',
        'email' => 'required|email:rfc,dns|unique:users',
        'password' => 'required|min:6'
    ];

    public function register()
    {
        $this->validate();

        try {
            \DB::beginTransaction();

            $user = User::create([
                'name' => $this->name,
                'email' => $this->email,
                'password' => Hash::make($this->password)
            ]);
            
            Account::create([
                'id'            => \Str::orderedUuid(),
                'account_number'=> random_int(100000000000000, 999999999999999),
                'user_id'       => $user->id,
                'expires'       => now()->addYears(5)->format('Y-m-d'),
            ]);

            \DB::commit();

            auth()->login($user);

        } catch (\Throwable $th) {
            \DB::rollback();

            \Log::error($th);
        }

        return redirect('/dashboard');
    }

    public function render()
    {
        return view('livewire.auth.register');
    }
}
