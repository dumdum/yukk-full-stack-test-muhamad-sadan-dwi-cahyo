<?php

namespace App\Http\Livewire;

use Livewire\Component;

use Illuminate\Validation\Rule;
use Livewire\Attributes\Rule as LRule; 
use Carbon\Carbon;
use App\Models\Transaction;
use App\Models\Account;
use Livewire\Attributes\On; 
use Livewire\WithPagination;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class Billing extends Component
{
    use WithPagination;

    public $historyWide     = 'full';
    public $isForm          = 'hidden';
    public $todayHistory    = [];
    public $accountList     = [];

    #[Rule('string|max:25')] 
    public $search          = '';
    public $filter          = '';
    public $today;
    public $accountNumber;
    public $balance;

    #[Locked] 
    public $account_id;
    public $type;
    public $amount;
    public $account_inv;
    public $note;

    public function rules()
    {
        return [
            'account_id'    => 'required|uuid|exists:accounts,id',
            'type'          => ['required', 'string', Rule::in(['out', 'top'])],
            'amount'        => 'required|numeric',
            'account_inv'   => 'nullable|uuid|required_if:type,out|exclude_if:type,top|exists:accounts,id',
            'note'          => 'nullable|string|max:255',
        ];
    }

    public function mount()
    {
        $this->account_id    = auth()->user()->account->id;
        $this->today         = Carbon::now()->locale('id')->translatedFormat('j F Y');
        $this->accountNumber = \Arr::join(str_split(auth()->user()->account->account_number, 3), '&nbsp;&nbsp;&nbsp;');
        $this->todayHistory  = Transaction::historyTransactions($this->search, $this->filter)->take(5)->get();
        $this->accountList   = Account::where('id', '<>', $this->account_id)->get();
    }

    public function boot()
    {
        $this->balance       = number_format(auth()->user()->account->balance);
        $this->amount = str_replace(',', '', $this->amount);
    }

    public function render()
    {
        return view('livewire.account.index', [
            "allHistory" => Transaction::historyTransactions($this->search, $this->filter)->paginate(3)
        ]);
    }

    public function toggleForm()
    {
        $this->historyWide = '7/12';
        $this->isForm      = 'open';
    }

    public function closeForm()
    {
        $this->reset([
            'historyWide', 
            'isForm',
            'type',
            'amount',
            'account_inv',
            'note'
        ]);
    }

    public function store()
    {
        $input = $this->validate();

        try {

            \DB::beginTransaction();

            $myAccount = $account = Account::lockValidAccountId($this->account_id, $input);

            /**
             * Transaksi Keluar
             * 
             * - Kurangi balance yang transfer
             * - Catat Transaksinya
             * 
             */
            if($this->type == "out") {
                $to           = Account::lockValidAccountId($this->account_inv, $input, false);
                $last_balance = ["last_balance" => $myAccount->balance];

                $myAccount->transactions()->outgoing(array_merge($input, $last_balance));
                $myAccount->decreaseBalance($myAccount, $input['amount']);

                $account = $to;
            }

            /**
             * Transaksi Masuk
             * 
             * - Tambahkan saldo bisa dari topup atau dari hasil transfer
             * - catat transaksinya topup/in
             * 
             */
    
            $account->transactions()->incoming(array_merge($input, [
                'last_balance' => $account->balance
            ]));
            $account->increaseBalance($account, $input['amount']);

            \DB::commit();


            $this->dispatch('complete', [
                'status'    => 'success',
                'message'   => 'Transaction Success.'
            ]);

        } catch (\Throwable $th) {
            $message = 'Oops, something went wrong.';

            \Log::error($th);

            \DB::rollback();

            if($th instanceof ModelNotFoundException) $message = 'Saldo kamu Kurang untuk melakukan transaksi';

            $this->dispatch('complete', [
                'status'    => 'fail',
                'message'   => $message
            ]);
        }
    }

    #[On('complete')] 
    public function complete($event)
    {
        return $event;
    }
}
