<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class UserHasAccount
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if(!auth()->user()->account()->exists()){
            return redirect()->route('dashboard')
                    ->with("error", 'Anda belum memiliki account.');
        }

        return $next($request);
    }
}
