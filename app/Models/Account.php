<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    use HasFactory;

    protected $primaryKey = 'id';
    public $incrementing = false;

    protected $table    = 'accounts';
    protected $fillable = [
        'user_id',
        'account_number',
        'expires',
        'balance',
    ];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->id = (string) \Str::orderedUuid();
        });
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class, 'account_id', 'id');
    }

    public function scopeLockValidAccountId($query, $id, $data, $isSender = true)
    {
        $query = $query->where('id', $id);

        if($data['type'] !== 'top' && $isSender) {
            $query = $query->where('balance', '>=', $data['amount']);
        }
                
        return $query->lockForUpdate()->firstOrFail();
    }

    public function decreaseBalance($myacc, $amount)
    {
        return $this->findOrFail($myacc->id)->update([
            'balance'   => (float) $myacc->balance - (float) $amount
        ]);
    }

    public function increaseBalance($acc, $amount)
    {
        return $this->findOrFail($acc->id)->update([
            'balance'   => (float) $acc->balance + (float) $amount
        ]);
    }
}
