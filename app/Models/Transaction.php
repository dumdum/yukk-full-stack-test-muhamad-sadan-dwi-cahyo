<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    protected $primaryKey = 'id';
    public $incrementing = false;

    protected $table    = 'transactions';
    protected $fillable = [
        'name',
        'type',
        'amount',
        'last_balance',
        'account_id',
        'account_inv',
        'note',
    ];

    public const transType = [
        'in'    => 'Transaksi Masuk',
        'out'   => 'Transaksi Keluar',
        'top'   => 'Top Up'
    ];

    public const transIcon = [
        'in'    => 'fas fa-arrow-down',
        'out'   => 'fas fa-arrow-up',
        'top'   => 'fas fa-donate'
    ];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->id = (string) \Str::orderedUuid();
        });
    }

    protected function createdAt(): Attribute
    {
        return Attribute::make(
            fn ($value) => \Carbon\Carbon::parse($value)->format('d-m-Y H:i'),
        );
    }

    public function scopeHistoryTransactions($query, $search, $filter)
    {
        $myAccount = auth()->user()->account->id;

        $query = $query->where('account_id', $myAccount);

        if(!empty($filter)) {
            $query = $query->where('type', $filter);
        }
        
        if(!empty($search)) {
            $query = $query->where(function($q) use($search) {
                return $q->where('name', 'like', "%$search%")
                        ->orWhere('note', 'like', "%$search%");
            });
        }

        return $query;
    }

    public function account()
    {
        return $this->belongsTo(Account::class, 'account_id');
    }

    public function toAccount()
    {
        return $this->belongsTo(Account::class, 'account_id');
    }

    public function scopeOutgoing($query, $input)
    {
        $title                 = self::transType[$input['type']];
        $input["name"]         = "$title Rp. $input[amount]";

        return $query->create($input);
    }

    public function scopeIncoming($query, $input)
    {
        if($input['type'] == 'out') {
            $input['type']  = 'in';
            list($input['account_id'], $input['account_inv']) = array($input['account_inv'], $input['account_id']);
        }

        $title                 = self::transType[$input['type']];
        $input["name"]         = "$title Rp. $input[amount]";

        return $query->create($input);
    }
}
