<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

use App\Models\User;
use App\Models\Account;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $user = User::factory()->create([
            'name' => 'Yukk Team',
            'email' => 'admin@admin.com',
            'password' => Hash::make('password'),
            'about' => "Hi, Yukk Team.",
        ]);

        Account::create([
            'id'            => \Str::orderedUuid(),
            'account_number'=> random_int(100000000000000, 999999999999999),
            'user_id'       => $user->id,
            'expires'       => now()->addYears(5)->format('Y-m-d'),
        ]);
    }
}
