<footer class="py-12">
  <div class="container">
    <div class="flex flex-wrap -mx-3">
      <div class="w-8/12 max-w-full px-3 mx-auto mt-1 text-center flex-0">
        <p class="mb-0 text-slate-400">
          Copyright ©
          <script>
            document.write(new Date().getFullYear());
          </script>
          Template by <a class="font-semibold text-slate-700" href="https://www.creative-tim.com" target="_blank">Creative Tim</a> & <a
            class="font-semibold text-slate-700" href="https://updivision.com" target="_blank">UPDIVISION</a>.
        </p>
      </div>
    </div>
  </div>
</footer>