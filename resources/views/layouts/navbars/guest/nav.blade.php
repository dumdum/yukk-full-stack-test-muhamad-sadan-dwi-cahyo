    <nav
        class="absolute top-0 z-30 flex flex-wrap items-center justify-between w-full px-4 py-2 mt-6 mb-4 lg:flex-nowrap lg:justify-start
            {{ Request::is('static-sign-up') || Request::is('register')
                ? 'shadow-none'
                : 'shadow-soft-2xl rounded-blur bg-white/80 backdrop-blur-2xl backdrop-saturate-200' }}">

        <div class="container flex items-center justify-between py-0 flex-wrap-inherit">

            {{-- <a class="py-2.375 text-size-sm mr-4 ml-4 whitespace-nowrap font-bold lg:ml-0 
            
            {{ Request::is('static-sign-up') || Request::is('register') ? 'text-white' : 'text-slate-700' }}"
                href="{{ url('dashboard') }}"> Yukk Dashboard </a> --}}

                <a class="block px-8 py-2 m-0 text-size-sm whitespace-nowrap text-slate-700" href="{{ url('') }}" target="_blank">
                    <img src="../assets/img/logos/yukk-favicon.ico"
                      class="inline h-full max-w-full transition-all duration-200 ease-nav-brand max-h-8" alt="main_logo" />
                    <span
                      class="{{ (Request::is('rtl') ? 'mr-1' : 'ml-1') }} font-semibold transition-all duration-200 ease-nav-brand">Yukk Test Dashboard</span>
                  </a>
        </div>
    </nav>