<div class="w-full max-w-full px-3 md:w-1/2 md:flex-none">
    <div
      class="relative flex flex-col min-w-0 break-words bg-white border-0 border-transparent border-solid shadow-soft-xl rounded-2xl bg-clip-border">
      <div
        class="p-4 mx-6 mb-0 text-center bg-white border-b-0 border-b-solid rounded-t-2xl border-b-transparent">
        <div class="w-16 h-16 text-center bg-center icon bg-gradient-fuchsia shadow-soft-2xl rounded-xl">
          <i class="relative text-white opacity-100 fas fa-wallet text-size-xl top-31/100"></i>
        </div>
      </div>
      <div class="flex-auto p-4 pt-0 text-center">
        <h6 class="mb-0 text-center">Balance</h6>
        <span class="leading-tight text-size-xs">Your Current Balance</span>
        <hr class="h-px my-4 bg-transparent bg-gradient-horizontal-dark" />
        <h5 class="mb-0">Rp. {{ $balance }}</h5>
      </div>
    </div>
</div>