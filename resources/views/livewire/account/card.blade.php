<div class="w-full max-w-full px-3 mb-4 xl:mb-0 xl:w-full xl:flex-none">
    <div
      class="relative flex flex-col min-w-0 break-words bg-transparent border-0 border-transparent border-solid shadow-xl rounded-2xl bg-clip-border">
      <div class="relative overflow-hidden rounded-2xl"
        style="background-image: url('../assets/img/curved-images/curved14.jpg')">
        <span
          class="absolute top-0 left-0 w-full h-full bg-center bg-cover bg-gradient-dark-gray opacity-80"></span>
        <div class="relative z-10 flex-auto p-4">
          <i class="p-2 text-white fas fa-wifi"></i>
          <h5 class="pb-2 mt-6 mb-12 text-white">
            {!! $accountNumber !!}  
          </h5>
          <div class="flex">
            <div class="flex">
              <div class="mr-6">
                <p class="mb-0 leading-normal text-white text-size-sm opacity-80">Card Holder</p>
                <h6 class="mb-0 text-white">{{ \Str::title(auth()->user()->name) }}</h6>
              </div>
              <div>
                <p class="mb-0 leading-normal text-white text-size-sm opacity-80">Expires</p>
                <h6 class="mb-0 text-white">{{ date('m/y', strtotime(auth()->user()->account->expires)) }}</h6>
              </div>
            </div>
            <div class="flex items-end justify-end w-1/5 ml-auto">
              <img class="w-3/5 mt-2" src="../assets/img/logos/mastercard.png" alt="logo" />
            </div>
          </div>
        </div>
      </div>
    </div>
</div>