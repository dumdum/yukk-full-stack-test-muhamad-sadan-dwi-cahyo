<div class="w-full max-w-full px-3 mt-6 md:w-5/12 md:flex-none {{ $isForm }}">
    <div class="w-full px-3 mb-6 e">
      <div class="relative flex flex-col min-w-0 break-words bg-white shadow-soft-xl rounded-2xl bg-clip-border">
          <div class="flex-auto p-4">

              <h5 class="font-bold py-3">Transaction Form</h5>

              <form wire:submit="store" wire:ignore>

                  <div class="flex flex-wrap -mx-3">
                      <div class="max-w-full px-3 w-full lg:flex-none" id="tipe">
                          <div class="flex flex-col h-full">
                              <h6 class="font-bold leading-tight uppercase text-size-xs text-slate-500 mandatory">
                                Tipe Transaksi
                              </h6>

                              <div class="mb-4">
                                <div class="inline-block relative w-full">
                                    <select class="block appearance-none w-full bg-white border border-gray-400 
                                        hover:border-gray-500 px-4 py-2 pr-8 rounded shadow leading-tight 
                                        focus:outline-none focus:shadow-outline"
                                        wire:model.blur='type'
                                        id="tipe-select">
                                      <option>Pilih Tipe</option>
                                      <option value="out">Transfer</option>
                                      <option value="top">Topup</option>
                                    </select>
                                    <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center 
                                        px-2 text-gray-700">
                                      <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" 
                                      viewBox="0 0 20 20">
                                        <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
                                      </svg>
                                    </div>
                                </div>

                                @error('type') <p class="text-size-sm text-red-500">{{ $message }}</p>
                                @enderror
                              </div>


                                <h6 class="font-bold leading-tight uppercase text-size-xs text-slate-500 mandatory">
                                Jumlah
                                </h6>

                                <div class="mb-4">
                                    <input wire:model.blur="amount" type="text"
                                        class="text-size-sm focus:shadow-soft-primary-outline leading-5.6 ease-soft 
                                            block w-full appearance-none rounded-lg border border-solid border-gray-300 
                                            bg-white bg-clip-padding py-2 px-3 font-normal text-gray-700 transition-all 
                                            focus:border-fuchsia-300 focus:bg-white focus:text-gray-700 
                                            focus:outline-none focus:transition-shadow money"
                                        placeholder="Amount" required />
                                    @error('amount') <p class="text-size-sm text-red-500">{{ $message }}</p>
                                    @enderror

                                </div>

                          </div>
                      </div>
                      <div class="max-w-full px-3 w-1/2 lg:flex-none hidden" id="tujuan">
                          <div class="flex flex-col h-full">
                              <h6 class="font-bold leading-tight uppercase text-size-xs text-slate-500 mandatory">
                                Tujuan Transfer
                              </h6>
                              <div class="mb-4">
                                <div class="inline-block relative w-full">
                                    <select class="block appearance-none w-full bg-white border border-gray-400 
                                        hover:border-gray-500 px-4 py-2 pr-8 rounded shadow leading-tight 
                                        focus:outline-none focus:shadow-outline"
                                        wire:model.blur='account_inv'>
                                        <option>Pilih Tujuan</option>

                                        @foreach ($accountList as $acc)
                                            <option value="{{ $acc->id }}">
                                                {{ "$acc->account_number - " . $acc->user->name }}
                                            </option>
                                        @endforeach

                                    </select>
                                    <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center 
                                        px-2 text-gray-700">
                                      <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" 
                                      viewBox="0 0 20 20">
                                        <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
                                      </svg>
                                    </div>
                                </div>  
                                
                                  @error('account_inv') <p class="text-size-sm text-red-500">{{ $message }}</p>
                                  @enderror

                              </div>

                          </div>
                      </div>
                  </div>
                  <h6 class="font-bold leading-tight uppercase text-size-xs text-slate-500">
                    Keterangan
                  </h6>
                  <div class="mb-4">
                      <textarea wire:model.blur="note" rows="3"
                          class="text-size-sm focus:shadow-soft-primary-outline leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding py-2 px-3 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:bg-white focus:text-gray-700 focus:outline-none focus:transition-shadow"
                          placeholder="Say something about yourself" id="user-about">  </textarea>
                      @error('note') <p class="text-size-sm text-red-500">{{ $message }}</p> @enderror
                  </div>
                  <div class="flow-root">
                    <button type="button" wire:click='closeForm'
                    class="float-right inline-block px-6 py-3 mt-6 ml-2 mb-2 font-bold text-center text-slate-700 uppercase align-middle transition-all bg-transparent border-0 rounded-lg cursor-pointer active:opacity-85 hover:scale-102 hover:shadow-soft-xs leading-pro text-size-xs ease-soft-in tracking-tight-soft shadow-soft-md bg-150 bg-x-25 hover:border-slate-700 hover:bg-red-500 hover:text-white">
                    Cancel</button>

                      <button type="submit"
                          class="float-right inline-block px-6 py-3 mt-6 mb-2 font-bold text-center text-white uppercase align-middle transition-all bg-transparent border-0 rounded-lg cursor-pointer active:opacity-85 hover:scale-102 hover:shadow-soft-xs leading-pro text-size-xs ease-soft-in tracking-tight-soft shadow-soft-md bg-150 bg-x-25 bg-gradient-dark-gray hover:border-slate-700 hover:bg-slate-700 hover:text-white">
                          Submit</button>
                  </div>
          </div>
          </form>
        </div>
    </div>
</div>
