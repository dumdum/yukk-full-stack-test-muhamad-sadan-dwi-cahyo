<div class="w-full max-w-full px-3 mt-6 md:w-{{ $historyWide }} md:flex-none">
    <div
      class="relative flex flex-col min-w-0 break-words bg-white border-0 shadow-soft-xl rounded-2xl bg-clip-border">

      <div class="p-4 pb-0 mb-0 bg-white border-b-0 border-b-solid rounded-t-2xl border-b-transparent">
        <div class="flex flex-wrap -mx-3">
          <div class="flex items-center flex-none w-1/2 max-w-full px-3">
            <h6 class="mb-0">History</h6>
          </div>
          <div class="flex-none w-1/2 max-w-full px-3 text-right" wire:click='toggleForm'>
            <a class="inline-block px-6 py-3 font-bold text-center text-white uppercase align-middle transition-all bg-transparent rounded-lg cursor-pointer leading-pro text-size-xs ease-soft-in shadow-soft-md bg-150 bg-gradient-dark-gray hover:shadow-soft-xs active:opacity-85 hover:scale-102 tracking-tight-soft bg-x-25"
              href="javascript:;"> <i class="fas fa-plus"> </i>&nbsp;&nbsp;New Transaction</a>
          </div>
        </div>
      </div>

      <div class="flex-auto p-4 pt-6">

          <h6 class="font-bold leading-tight uppercase text-size-xs text-slate-500">
          Search
          </h6>
          <div class="mb-4">
            <input wire:model.live="search" type="text"
            class="text-size-sm focus:shadow-soft-primary-outline leading-5.6 ease-soft 
                block w-1/4 appearance-none rounded-lg border border-solid border-gray-300 
                bg-white bg-clip-padding py-2 px-3 font-normal text-gray-700 transition-all 
                focus:border-fuchsia-300 focus:bg-white focus:text-gray-700 
                focus:outline-none focus:transition-shadow"
            placeholder="Search ..." />
          </div>

          <h6 class="font-bold leading-tight uppercase text-size-xs text-slate-500">
            Tipe Transaksi
          </h6>
          <div class="mb-4">
            <div class="inline-block relative w-1/4">
                <select class="block appearance-none w-full bg-white border border-gray-400 
                    hover:border-gray-500 px-4 py-2 pr-8 rounded shadow leading-tight 
                    focus:outline-none focus:shadow-outline"
                    wire:model.live='filter'>
                  <option value="">Pilih Tipe</option>
                  <option value="out">Transfer</option>
                  <option value="top">Topup</option>
                </select>
                <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center 
                    px-2 text-gray-700">
                  <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" 
                  viewBox="0 0 20 20">
                    <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
                  </svg>
                </div>
            </div>
          </div>

        <ul class="flex flex-col pl-0 mb-0 rounded-lg">

          @forelse ($allHistory as $all)

            <li class="relative flex p-6 mb-2 border-0 rounded-t-inherit rounded-xl bg-gray-50">
              <div class="flex flex-col">
                <h6 class="mb-4 leading-normal text-size-sm">{{ $all->name }}</h6>
                <span class="mb-2 leading-tight text-size-xs">Tipe Transaksi: <span
                    class="font-semibold text-slate-700 sm:ml-2">{{ \App\Models\Transaction::transType[$all->type] }}</span></span>
                <span class="mb-2 leading-tight text-size-xs">Jumlah: <span
                    class="font-semibold text-slate-700 sm:ml-2">Rp. {{ number_format($all->amount) }}</span></span>
                <span class="leading-tight text-size-xs">Keterangan: <span
                    class="font-semibold text-slate-700 sm:ml-2">{{ $all->keterangan }}</span></span>
              </div>
              <div class="ml-auto text-right">
                <a class="relative z-10 inline-block px-4 py-3 mb-0 font-bold text-center text-transparent uppercase align-middle transition-all border-0 rounded-lg shadow-none cursor-pointer leading-pro text-size-xs ease-soft-in bg-150 bg-gradient-red hover:scale-102 active:opacity-85 bg-x-25 bg-clip-text"
                  href="javascript:;"><i
                    class="mr-2 far fa-trash-alt bg-150 bg-gradient-red bg-x-25 bg-clip-text"></i>Delete</a>
                <a class="inline-block px-4 py-3 mb-0 font-bold text-center uppercase align-middle transition-all bg-transparent border-0 rounded-lg shadow-none cursor-pointer leading-pro text-size-xs ease-soft-in bg-150 hover:scale-102 active:opacity-85 bg-x-25 text-slate-700"
                  href="javascript:;"><i class="mr-2 fas fa-pencil-alt text-slate-700" aria-hidden="true"></i>Edit</a>
              </div>
            </li>

          @empty

            <li
            class="relative flex justify-between px-4 py-2 pl-0 mb-2 bg-white border-0 rounded-t-inherit text-size-inherit rounded-xl">
                <span class="leading-tight text-size-xs">No Data Available ... </span>
            </li>

          @endforelse

        </ul>

        {{ $allHistory->links() }}
      </div>
    </div>
</div>