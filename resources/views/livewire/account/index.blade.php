<div>
    <div class="w-full p-6 mx-auto">
      <div class="flex flex-wrap -mx-3">
        <div class="max-w-full mb-4 px-3 lg:w-1/2 lg:flex-none">
          <div class="flex flex-wrap -mx-3">

            @include('livewire.account.card')

            <div class="w-full max-w-full mb-8 px-3 mt-4 xl:w-full xl:flex-none">
              <div class="flex flex-wrap -mx-3">

                @include('livewire.account.balance')

                @include('livewire.account.total')

              </div>
            </div>
  
          </div>
        </div>

        @include('livewire.account.todayHistory')

      </div>
  
      <div class="flex flex-wrap -mx-3">

        @include('livewire.account.historyTable')
  
        @include('livewire.account.form')

      </div>
    </div>
    
    <script>
      document.addEventListener('livewire:init', () => {
  
          $('.money').simpleMoneyFormat();
  
          $('#tipe-select').on('change', function() {
              let idTipe = $('#tipe');
              let tujuan = $('#tujuan');
  
              if($(this).val() == 'out') {
                  tujuan.removeClass('hidden');
                  idTipe.removeClass('w-full');
                  idTipe.addClass('w-1/2');
              }else{
                  tujuan.addClass('hidden');
                  idTipe.removeClass('w-1/2');  
                  idTipe.addClass('w-full');
              }
          });
  
      })
  
  </script>

<script>
  document.addEventListener('livewire:initialized', () => {
    Livewire.on('complete', (ev) => {
      console.log(ev[0].status)
      let type = ev[0].status === 'fail' ? 'error' : 'success';
      let title = ev[0].status === 'fail' ? 'Oops!' : 'Berhasil';

      $.toast({
          heading: title,
          text: ev[0].message,
          showHideTransition: 'fade',
          icon: type
      })
    });
  });
</script>
  

  </div>

