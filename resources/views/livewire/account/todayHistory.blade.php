<div class="w-full max-w-full px-3 lg:w-1/2 lg:flex-none">
    <div class="relative flex flex-col h-full min-w-0 mb-6 break-words bg-white border-0 shadow-soft-xl rounded-2xl bg-clip-border">
      <div class="p-6 px-4 pb-0 mb-0 bg-white border-b-0 rounded-t-2xl">
        <div class="flex flex-wrap -mx-3">
          <div class="max-w-full px-3 md:w-1/2 md:flex-none">
            <h6 class="mb-0">Your Transactions</h6>
          </div>
          <div class="flex items-center justify-end max-w-full px-3 md:w-1/2 md:flex-none">
            <i class="mr-2 far fa-calendar-alt"></i>
            <small>{{ $today }}</small>
          </div>
        </div>
      </div>
      <div class="flex-auto p-4 pt-6">
        <h6 class="mb-4 font-bold leading-tight uppercase text-size-xs text-slate-500">Newest</h6>
        <ul class="flex flex-col pl-0 mb-0 rounded-lg">

            @forelse ($todayHistory as $item)
                <li
                class="relative flex justify-between px-4 py-2 pl-0 mb-2 bg-white border-0 
                    rounded-t-inherit text-size-inherit rounded-xl">
                <div class="flex items-center">
                    <button
                    class="leading-pro ease-soft-in text-size-xs bg-150 w-6.35 h-6.35 p-1.2 
                        rounded-3.5xl tracking-tight-soft bg-x-25 mr-4 mb-0 flex cursor-pointer 
                        items-center justify-center border border-solid border-red-600 border-transparent 
                        bg-transparent text-center align-middle font-bold uppercase 
                        {{ $item->type == "out" ? 'text-red-600' : 'text-lime-500' }} transition-all hover:opacity-75">

                        <i class="{{ \App\Models\Transaction::transIcon[$item->type] }} text-sm"></i>

                    </button>
                    <div class="flex flex-col">
                    <h6 class="mb-1 leading-normal text-size-sm text-slate-700">{{ $item->name }}</h6>
                    <span class="leading-tight text-size-xs">{{ $item->created_at }}</span>
                    </div>
                </div>
                <div class="flex flex-col items-center justify-center">
                    <p
                    class="relative z-10 inline-block m-0 font-semibold leading-normal text-transparent 
                    {{ $item->type == 'out' ? 'bg-gradient-red' : 'bg-gradient-lime' }} text-size-sm bg-clip-text">
                    {{ $item->type == 'out' ? '-':'+'  }} Rp. {{ number_format($item->amount) }}</p>
                </div>
                </li>

            @empty
                <li
                class="relative flex justify-between px-4 py-2 pl-0 mb-2 bg-white border-0 rounded-t-inherit text-size-inherit rounded-xl">
                    <span class="leading-tight text-size-xs">No Data Available ... </span>
                </li>


            @endforelse

        </ul>
      </div>
    </div>
</div>